/*
 * ifwlog-test: A test program for the ipt_IFWLOG kernel module
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <linux/if.h>
#include <linux/netlink.h>

#define NETLINK_IFWLOG  20
#define IFWLOG_GRP       1

// XXX: Copied from kernel sources, is there something
// like that for user space?
#define NIPQUAD(addr) \
	((unsigned char *)&addr)[0], \
	((unsigned char *)&addr)[1], \
	((unsigned char *)&addr)[2], \
	((unsigned char *)&addr)[3]

#define PREFSIZ		32

struct nl_msg {				/* Netlink kernel to user message */
	long timestamp_sec;		/* time packet */
	char indev_name[IFNAMSIZ];	/* name of the ingoing interface */
	char outdev_name[IFNAMSIZ];	/* name of the outgoing interface */
	unsigned char prefix[PREFSIZ];	/* logging informations */
	struct iphdr ip;
	union {
		struct tcphdr th;
		struct udphdr uh;
	} h;
};

/* create_socket(): Create a netlink socket and bind it.
 * Return a file-descriptor on success, -1 otherwise. */
static int create_socket(void)
{
	int err, fd;
	struct sockaddr_nl sa;

	fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_IFWLOG);
	if (fd < 0)
		return -1;

	memset(&sa, 0, sizeof(sa));
	sa.nl_family = AF_NETLINK;
	sa.nl_pid = getpid();        // self pid
	sa.nl_groups = IFWLOG_GRP;   // group

	err = bind(fd, (struct sockaddr *) &sa, sizeof(sa));
	if (err)
		return -1;

	return fd;
}

/* read_message(): Read a netlink message from 'sock_fd'
 * and return it. */
static struct nlmsghdr *read_message(int sock_fd)
{
	ssize_t len, bytes;
	struct iovec iov;
	struct msghdr msg;
	struct nlmsghdr *nlhdr;
	struct sockaddr_nl nladdr;

	len = NLMSG_SPACE(sizeof(struct nl_msg));
	nlhdr = malloc(len);
	if (!nlhdr)
		return NULL;

	memset(nlhdr, 0, len);
	iov.iov_base = (void *) nlhdr;
	iov.iov_len  = len;

	memset(&nladdr, 0, sizeof(nladdr));

	memset(&msg, 0, sizeof(msg));
	msg.msg_name = (void *) &(nladdr);
	msg.msg_namelen = sizeof(nladdr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	bytes = recvmsg(sock_fd, &msg, 0);
	if (bytes < 0) {
		free(nlhdr);
		return NULL;
	}

	return nlhdr;
}

static void dump_message(const struct nl_msg *p)
{
	printf("prefix: %s\n", p->prefix);
	if (p->indev_name[0])
		printf("in: %s\n", p->indev_name);
	if (p->outdev_name[0])
		printf("out: %s\n", p->outdev_name);
	printf("source ip: %d.%d.%d.%d\n", NIPQUAD(p->ip.saddr));
	printf("dest   ip: %d.%d.%d.%d\n", NIPQUAD(p->ip.daddr));
	printf("\n");
}

int main(int argc, char *argv[])
{
	int i, max, sock_fd;
	struct nlmsghdr *msg;
	struct nl_msg nl_msg;

	if (argc == 1) {
		printf("usage: ifwlog-test < nr-messages >\n");
		exit(0);
	}

	sock_fd = create_socket();
	if (sock_fd < 0) {
		perror("create_socket()");
		exit(1);
	}

	max = strtol(argv[1], NULL, 10);

	for (i = 0; i < max; i++) {
		msg = read_message(sock_fd);
		if (!msg) {
			perror("read_message()");
			exit(1);
		}

		memcpy(&nl_msg, NLMSG_DATA(msg), sizeof(nl_msg));
		dump_message(&nl_msg);
		free(msg);
	}

	close(sock_fd);
	return 0;
}
