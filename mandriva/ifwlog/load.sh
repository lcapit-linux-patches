#!/bin/sh

iptables -F
rmmod ipt_IFWLOG
insmod ./ipt_IFWLOG.ko
iptables -A INPUT -j IFWLOG --log-prefix papagali
iptables -A OUTPUT -j IFWLOG --log-prefix papagali
